﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.RemoteData
{
    public static class HttpConsts
    {
        public const string ApplicationJsonHeaderValue = "application/json";
        public const string Bearer = "Bearer";
        public static string Accept = "Accept";
    }
}
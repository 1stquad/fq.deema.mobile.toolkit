﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.RemoteData
{
    public class ErrorDescription
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string DetailedErrorCode { get; set; }
    }
}
﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Stripe
{
    public class StripeConfig
    {
        public string ApiKey { get; set; }
        public string Endpoint { get; set; }
    }
}
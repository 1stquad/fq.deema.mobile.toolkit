﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

using System;
using Softeq.XToolkit.Common.EventArguments;

namespace Softeq.XToolkit.Connectivity.Reachability.iOS
{
    public class ConnectivityManager : IConnectivityManager
    {
        private readonly ReachabilityBindings.Reachability _reachability;
        
        public event EventHandler<GenericEventArgs<bool>> InternetConnectionChanged;

        public ConnectivityManager()
        {
            _reachability = ReachabilityBindings.Reachability.ReachabilityForInternetConnection;

            _reachability.ReachableBlock = (ReachabilityBindings.Reachability reachability) =>
            {
                InternetConnectionChanged?.Invoke(this, new GenericEventArgs<bool>(reachability.IsReachable));
            };

            _reachability.UnreachableBlock = (ReachabilityBindings.Reachability reachability) =>
            {
                InternetConnectionChanged?.Invoke(this, new GenericEventArgs<bool>(reachability.IsReachable));
            };

            _reachability.StartNotifier();
        }

        public bool IsConnected => _reachability.IsReachable;
    }
}

﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Permissions
{
    public enum PermissionStatus
    {
        Granted,
        Denied,
        Unknown
    }
}

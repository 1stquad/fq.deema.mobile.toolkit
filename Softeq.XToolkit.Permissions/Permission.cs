﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Permissions
{
    public enum Permission
    {
        Photos,
        Camera,
        Storage,
        Notifications
    }
}

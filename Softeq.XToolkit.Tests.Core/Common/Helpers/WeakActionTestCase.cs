﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Tests.Core.Common.Helpers
{
    public enum WeakActionTestCase
    {
        PublicNamedMethod,
        InternalNamedMethod,
        PrivateNamedMethod,
        PublicStaticMethod,
        InternalStaticMethod,
        PrivateStaticMethod,
        AnonymousMethod,
        AnonymousStaticMethod
    }
}
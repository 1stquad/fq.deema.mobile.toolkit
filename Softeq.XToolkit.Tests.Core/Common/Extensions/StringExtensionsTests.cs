﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

using System;
using System.Linq;
using Softeq.XToolkit.Common.Extensions;
using Xunit;

namespace Softeq.XToolkit.Tests.Core.Common.Extensions
{
    public class StringExtensionsTests
    {
        [Theory]
        [InlineData("", 0)]
        [InlineData("https://фывафыва.com", 1)]
        [InlineData("https://softeq.com", 1)]
        [InlineData("ftp://softeq.com", 1)]
        [InlineData("ftp://127.0.0.1", 1)]
        [InlineData("ftps://softeq.com", 1)]
        [InlineData("someproto://softeq.com", 1)]
        [InlineData("text htTp://softeq.com?args=123", 1)]
        [InlineData("htTp://softeq.com?args=123 text", 1)]
        [InlineData("http:// empty htTp://softeq.com?args=123 text goole.com", 1)]
        [InlineData("some text htTp://softeq.com?args=123 text text http://google.com text", 2)]
        public void FindLinksForCorrectStringTest(string text, int linksCount)
        {
            var links = StringExtensions.FindLinks(text).ToArray();

            Assert.Equal(linksCount, links.Length);
        }

        [Theory]
        [InlineData("some text htTp://softeq.com?args=123 text text http://google.com text", 10, 26)]
        [InlineData("some :// http: HtTp://1234-234.com text text http://google.com text", 15, 19)]
        public void FindLinksWithCheckFirstLinkTest(string text, int firstLinkIndex, int firstLinkLength)
        {
            var links = StringExtensions.FindLinks(text).ToArray();
            var firstLink = links[0];

            Assert.Equal(firstLinkIndex, firstLink.Index);
            Assert.Equal(firstLinkLength, firstLink.Length);
        }

        [Theory]
        [InlineData(null, typeof(ArgumentNullException))]
        public void FindLinksForInCorrectStringTest(string text, Type exceptionType)
        {
            Assert.Throws(exceptionType, () =>
            {
                var links = StringExtensions.FindLinks(text).ToArray();
            });
        }
    }
}

﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Softeq.XToolkit.Common.Extensions
{
    public static class StringExtensions
    {
        public static IEnumerable<Capture> FindLinks(this string self)
        {
            var linkPattern = @"[a-z]+://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?";

            foreach (Capture match in Regex.Matches(self, linkPattern, RegexOptions.IgnoreCase))
            {
                yield return match;
            }
        }
    }
}

﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

using System;
using Softeq.XToolkit.Common.EventArguments;

namespace Softeq.XToolkit.Connectivity
{
    public interface IConnectivityManager
    {
        event EventHandler<GenericEventArgs<bool>> InternetConnectionChanged;
        bool IsConnected { get; }
    }
}
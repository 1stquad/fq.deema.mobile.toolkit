﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

namespace Softeq.XToolkit.Auth.Internal.Dtos
{
    internal class RegisterDto
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
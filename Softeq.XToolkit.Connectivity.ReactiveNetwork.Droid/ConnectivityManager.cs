﻿// Developed by Softeq Development Corporation
// http://www.softeq.com

using System;
using System.Collections.Generic;
using Android.App;
using Softeq.XToolkit.Common.EventArguments;

namespace Softeq.XToolkit.Connectivity.ReactiveNetwork.Droid
{
    public class ConnectivityManager : Java.Lang.Object, IO.Reactivex.Functions.IConsumer, IConnectivityManager 
    {
        private readonly Dictionary<string, IDisposable> _subscriptions;

        public event EventHandler<GenericEventArgs<bool>> InternetConnectionChanged;

        public ConnectivityManager()
        {
            _subscriptions = new Dictionary<string, IDisposable>();
        }

        public void Subscribe(Activity activity)
        {
            var (Key, Subscription) = GetFromDict(activity);
            Subscription?.Dispose();

            var newSubscription = Com.Github.Pwittchen.Reactivenetwork.Library.Rx2.ReactiveNetwork
            .ObserveNetworkConnectivity(activity)
            .SubscribeOn(IO.Reactivex.Schedulers.Schedulers.Io())
            .Subscribe(this);

            _subscriptions[Key] = newSubscription;
        }

        public void UnSubscribe(Activity activity)
        {
            var (Key, Subscription) = GetFromDict(activity);

            if (Subscription != null)
            {
                Subscription.Dispose();
                _subscriptions.Remove(Key);
            }

            if (_subscriptions.Count == 0)
            {
                InternetConnectionChanged?.Invoke(this, new GenericEventArgs<bool>(true));
            }
        }

        public bool IsConnected { get; private set; } = true;

        public void Accept(Java.Lang.Object p0)
        {
            if (p0 is Com.Github.Pwittchen.Reactivenetwork.Library.Rx2.Connectivity connectivity)
            {
                IsConnected = connectivity.State() == Android.Net.NetworkInfo.State.Connected;
                InternetConnectionChanged?.Invoke(this, new GenericEventArgs<bool>(IsConnected));
            }
        }

        private (string Key, IDisposable Subscription) GetFromDict(Activity activity)
        {
            var key = activity.GetType().FullName;
            _subscriptions.TryGetValue(key, out IDisposable result);
            return (key, result);
        }
    }
}
